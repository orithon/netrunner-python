class Stack(list):
    def push(self, item):
        try:
            items = [item] if isinstance(item, str) else list(item)
        except TypeError:
            items = [item]
        items.reverse()
        for i in items:
            self.append(i)

    def peek(self):
        return self[-1]


class Choice(object):
    def __init__(self):
        self.choices = []

    def __contains__(self, message):
        return message in self.choices

    def __bool__(self):
        return bool(self.choices)

    def __str__(self):
        return 'Choice: {}'.format(str(self.choices))

    def add(self, message):
        self.choices.append(message)


class Ordering(Choice):
    def remove(self, message):
        self.choices.remove(message)

    def __str__(self):
        return 'Ordering: {}'.format(str(self.choices))
