from uuid import uuid4

def uidgen():
    uids = []
    while True:
        uid = str(uuid4())
        if uid not in uids:
            uids.append(uid)
            yield uid
