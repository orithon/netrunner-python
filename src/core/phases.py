class Phase(object):
    def __init__(self, name, *messages):
        self.name = name
        self.start = messages[0]
        self.end = messages[-1]
        self.body = list(messages[1:-1])

    def getMessages(self):
        return [self.start] + self.body + [self.end]

    def getBody(self):
        return self.body


class PhaseManager(object):
    def __init__(self):
        self.phases = []
        self.current = -1;

    def addPhase(self, phase):
        self.phases.append(phase)

    def getNextPhase(self):
        if not self.phases:
            raise IndexError('The {} has no phases'.format(self.__class__.__name__))
        self.current = (self.current + 1) % len(self.phases)
        return self.phases[self.current]
