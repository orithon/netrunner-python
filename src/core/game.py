from core.messaging import Messenger, Message, Subscriptions
from core.stack import Stack, Choice, Ordering
from core.phases import PhaseManager
from core.uids import uidgen


class InvalidMoveError(Exception): pass
class GameWon(Exception): pass


class Game(Messenger):
    def __init__(self):
        super().__init__('Game', Subscriptions())
        self.winner = None
        self.uidgen = uidgen()
        self.stack = Stack()
        self.phaseManager = PhaseManager()
        self.initializePhases()
        self.initializePlayers()
        self.initializeSubscriptions()

    def initializePhases(self): pass
    def initializePlayers(self): pass
    def initializeSubscriptions(self): pass

    def log(self, message, player=None):
        '''Log a message for the specified player. If no player is specified the
        message is logged to both players.'''
        print(message)

    def incrementPhase(self):
        self.phase = self.phaseManager.getNextPhase()
        self.stack.push(self.phase.getMessages())

    def handleOrdering(self, ordering, message):
        if message not in ordering:
            raise InvalidMoveError
        ordering.remove(message)
        if ordering:
            self.stack.push(ordering)
        self.stack.push(message)

    def handleChoice(self, choice, message):
        if message not in choice:
            raise InvalidMoveError
        self.stack.push(message)

    def pushMessage(self, message):
        self.stack.push(message)

    def _pushThing(self, cls, message):
        thing = self.stack.peek()
        if not isinstance(thing, cls):
            thing = cls()
            self.stack.push(thing)
        thing.add(message)

    def addChoice(self, message):
        return self._pushThing(Choice, message)

    def addOrdering(self, message):
        return self._pushThing(Ordering, message)

    def process(self, message=None):
        if self.winner is not None:
            raise GameWon()

        if message is not None:
            # Pop the top off the stack. If it's an ordering, and the message is
            # in it, then:
            #     1. Remove the message from the ordering.
            #     2. If the ordering has messages, push it on the stack.
            #     3. Push the message on the stack.
            # If it's a choice, and the message is in it, then:
            #     1. Push the message on the stack.
            top = self.stack.pop()
            if isinstance(top, Ordering):
                self.handleOrdering(top, message)
            elif isinstance(top, Choice):
                self.handleChoice(top, message)
            else:
                raise InvalidMoveError

        while True:
            try:
                top = self.stack.pop()
            except IndexError:
                self.incrementPhase()
                continue

            if isinstance(top, (Choice, Ordering)):
                self.stack.push(top)
                return top

            self.send(top)
