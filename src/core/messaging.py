from operator import attrgetter

from core.uids import uidgen


class Priority(object):
    HIGH = 1
    MEDIUM = 2
    LOW = 3


class Subscriptions(dict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.uidgen = uidgen()

    def add(self, subscription):
        message = subscription.message
        if message not in self:
            self[message] = []
        if subscription in self[message]:
            raise ValueError('Subscriber is already subscribed to that message')
        self[message].append(subscription)
        self[message].sort(key=attrgetter('message'))

    def remove(self, subscription):
        message = subscription.message
        try:
            self[message].remove(subscription)
            if not self[message]:
                del self[message]
        except KeyError:
            raise KeyError('No subscriptions for that message')
        except ValueError:
            raise ValueError('Can only unsubscribe subscribers')

    def send(self, message):
        try:
            for subscription in self[message]:
                if subscription.matches(message):
                    subscription(message)
        except KeyError:
            pass


class Subscription(object):
    def __init__(self, message, sender=None, subscriber=None,
                 priority=Priority.MEDIUM):
        self.message = message
        self.sender = sender
        self.subscriber = subscriber
        self.priority = priority

    def __str__(self):
        return 'Subscription(%s, %s, %s, %s)' % (
            self.message, self.sender, self.subscriber, self.priority
        )

    def __repr__(self):
        return str(self)

    def __call__(self, message):
        if self.subscriber is None:
            raise NotImplementedError
        self.subscriber(message)

    def __eq__(self, other):
        return (
            isinstance(other, Subscription) and
            self.message == other.message and
            self.sender == other.sender and
            self.subscriber == other.subscriber
        )

    def matches(self, message):
        return (
            self.message == message and (
                self.sender is None or self.sender == message.sender
            )
        )


class Message(object):
    def __init__(self, message, sender, data=None):
        self.message = message
        self.sender = sender
        self.data = data

    def __str__(self):
        return '<Message message="%s" sender=%s data=%s>' % (
            self.message, self.sender, self.data
        )


class Messenger(object):
    def __init__(self, name, subscriptions):
        self.subscriptions = subscriptions
        self.name = name
        self.uid = next(subscriptions.uidgen)

    def __eq__(self, other):
        return isinstance(other, Messenger) and self.uid == other.uid

    def __str__(self):
        return '%s - %s' % (self.name, self.uid)

    def subscribe(self, method, message, sender=None, priority=Priority.MEDIUM):
        self.subscriptions.add(Subscription(message, sender, method, priority))

    def unsubscribe(self, message, sender=None):
        self.subscriptions.remove(Subscription(message, sender))

    def createMessage(self, message, data=None):
        return Message(message, self, data)

    def send(self, message):
        print('Firing', message)
        self.subscriptions.send(message)
        return message
