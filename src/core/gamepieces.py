from core.messaging import Messenger


class Piece(Messenger):
    def __init__(self, name:str, game, player=None):
        super().__init__(name, game.subscriptions)
        self.name = name
        self.game = game
        self.player = player
        self.parent = None
        self.zone = None
        self.zones = {}
        self.initializeSubscriptions()

    def __hash__(self):
        return int(self.uid.replace('-', ''), base=16)

    def __str__(self):
        return self.name.title()

    def __eq__(self, other):
        return (isinstance(other, Piece) and
                self.name == other.name and
                self.uid == other.uid)

    def __getitem__(self, item):
        return self.zones[item]

    def initializeSubscriptions(self):
        pass

    def jsonify(self):
        'Convert to a format that can easily turned into a JSON string'
        return {'name': self.name,
                'uid': self.uid,
                'subpieces': {
                    z: [p.jsonify() for p in self[z]] for z in self.zones
                }}

    def removeFromPlay(self):
        'Remove the piece, and all of its subpieces, from play'
        self.parent._removePiece(self)
        self.player = None
        self.parent = None
        self.zone = None
        for _, pieces in self.zones.items():
            for piece in pieces:
                piece.removeFromPlay()

    def hasPiece(self, piece) -> bool:
        'Returns True if the given piece is a subpiece of this piece'
        for _, pieces in self.zones.items():
            if piece in pieces:
                return True
        return False

    def move(self, to, zone:str):
        'Move this piece from its parent to the given piece in the given zone'
        if self.parent is not None:
            self.parent._removePiece(piece)
        to._addPiece(self, zone)
        self.parent = to
        self.zone = zone

    def addZone(self, zone:str):
        'Register a zone with the given name in the subpieces dictionary'
        if self.hasZone(zone):
            msg = 'Piece %s already has zone %s'
            raise ValueError(msg % (self, zone))
        self.zones[zone] = []

    def hasZone(self, zone:str) -> bool:
        'Returns True if the piece has the given zone'
        return zone in self.zones

    def getAmount(self, zone:str) -> int:
        'Returns the number of subpieces in the given zone'
        return len(self.zones[zone])

    def _addPiece(self, piece, zone:str):
        if self.hasPiece(piece):
            msg = 'Piece %s already has subpiece %s in zone %s'
            raise ValueError(msg % (self, piece, zone))
        self.zones[zone].append(piece)

    def _removePiece(self, piece):
        for zone, pieces in self.zones.items():
            if piece in pieces:
                self.zones[zone].remove(piece)
                return
        msg = 'Piece %s does not have subpiece %s'
        raise ValueError(msg % (self, piece))
