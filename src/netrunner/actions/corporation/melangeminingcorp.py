from netrunner.actions.corporation import Action
from nerunner.events import CORP_GAIN_CREDIT


class MelangeMiningCorp(Action):
    cost = 3
    trigger = 'MELANGE_MINING_CORP'

    def add(self, event):
        super().add(event)
        self.game.addChoice(self.trigger, self.act)

    def act(self, event):
        super().act(event)
        message = self.createMessage(CORP_GAIN_CREDIT, data=7)
        self.game.pushMessage(message)
