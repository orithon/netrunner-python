from netrunner.actions.corporation import Action


class PrivateSecurityForce(Action):
    trigger = 'PRIVATE_SECURITY_FORCE'

    def add(self, event):
        super().add(event)
        if self.game.runnerIsTagged():
            self.game.addChoice(self.trigger, self.act)

    def act(self, event):
        super().act(event)
        self.game.dealMeatDamage(1)
