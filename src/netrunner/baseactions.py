from core.gamepieces import Piece
from netrunner.events import *


RUNNER_GAIN_CREDIT_ACTION = 'RUNNER_GAIN_CREDIT_ACTION'
RUNNER_RUN_ACTION = 'RUNNER_RUN_ACTION'
RUNNER_DRAW_ACTION = 'RUNNER_DRAW_ACTION'
RUNNER_REMOVE_TAG_ACTION = 'RUNNER_REMOVE_TAG_ACTION'

CORP_GAIN_CREDIT_ACTION = 'CORP_GAIN_CREDIT_ACTION'
CORP_DRAW_ACTION = 'CORP_DRAW_ACTION'
CORP_ADVANCE_ACTION = 'CORP_ADVANCE_ACTION'
CORP_PURGE_ACTION = 'CORP_PURGE_ACTION'
CORP_TRASH_RESOURCE_ACTION = 'CORP_TRASH_RESOURCE_ACTION'


class RunnerActions(Piece):
    def __init__(self, game, player):
        super().__init__('runner actions', game, player)

    def initializeSubscriptions(self):
        self.subscribe(self.addActions, RUNNER_ACTION)
        self.subscribe(self.handleGainCredit, RUNNER_GAIN_CREDIT_ACTION)
        self.subscribe(self.handleRun, RUNNER_RUN_ACTION)
        self.subscribe(self.handleDraw, RUNNER_DRAW_ACTION)
        self.subscribe(self.handleRemoveTag, RUNNER_REMOVE_TAG_ACTION)

    def addActions(self, event):
        game = self.game
        player = self.player
        game.addChoice(RUNNER_GAIN_CREDIT_ACTION)
        game.addChoice(RUNNER_RUN_ACTION)

        if player.getAmountStack() > 0:
            game.addChoice(RUNNER_DRAW_ACTION)
        if player.getAmountTags() > 0 and player.getAmountCredits() >= 2:
            game.addChoice(RUNNER_REMOVE_TAG_ACTION)

    def handleGainCredit(self, event):
        self.game.pushMessage(self.createMessage(RUNNER_GAIN_CREDIT))
        self.game.pushMessage(self.createMessage(RUNNER_LOSE_CLICK))

    def handleRun(self, event):
        self.game.pushMessage(self.createMessage(RUNNER_RUN))
        self.game.pushMessage(self.createMessage(RUNNER_LOSE_CLICK))

    def handleDraw(self, event):
        self.game.pushMessage(self.createMessage(RUNNER_DRAW))
        self.game.pushMessage(self.createMessage(RUNNER_LOSE_CLICK))

    def handleRemoveTag(self, event):
        self.game.pushMessage(self.createMessage(RUNNER_REMOVE_TAG))
        self.game.pushMessage(self.createMessage(RUNNER_LOSE_CREDIT, data=2))
        self.game.pushMessage(self.createMessage(RUNNER_LOSE_CLICK))


class CorporationActions(Piece):
    def __init__(self, game, player):
        super().__init__('corporation actions', game, player)

    def initializeSubscriptions(self):
        self.subscribe(self.addActions, CORP_ACTION)
        self.subscribe(self.handleGainCredit, CORP_GAIN_CREDIT_ACTION)
        self.subscribe(self.handleDraw, CORP_DRAW_ACTION)
        self.subscribe(self.handleAdvance, CORP_ADVANCE_ACTION)
        self.subscribe(self.handlePurge, CORP_PURGE_ACTION)
        self.subscribe(self.handleTrashResource, CORP_TRASH_RESOURCE_ACTION)

    def addActions(self, event):
        game = self.game
        player = self.player
        game.addChoice(CORP_GAIN_CREDIT_ACTION)

        if player.getAmountRD() > 0:
            game.addChoice(CORP_DRAW_ACTION)
        if player.getAmountCredits() > 0 and game.getAmountAdvanceable():
            game.addChoice(CORP_ADVANCE_ACTION)
        if game.getAmountVirusCounters() > 0:
            game.addChoice(CORP_PURGE_ACTION)
        if game.runner.getAmountTags() > 0:
            game.addChoice(CORP_TRASH_RESOURCE_ACTION)

    def handleGainCredit(self, event):
        self.game.pushMessage(self.createMessage(CORP_GAIN_CREDIT))
        self.game.pushMessage(self.createMessage(CORP_LOSE_CLICK))

    def handleDraw(self, event):
        self.game.pushMessage(self.createMessage(CORP_DRAW))
        self.game.pushMessage(self.createMessage(CORP_LOSE_CLICK))

    def handleAdvance(self, event):
        self.game.pushMessage(self.createMessage(CORP_ADVANCE))
        self.game.pushMessage(self.createMessage(CORP_LOSE_CREDIT))
        self.game.pushMessage(self.createMessage(CORP_LOSE_CLICK))

    def handlePurge(self, event):
        self.game.pushMessage(self.createMessage(CORP_PURGE))
        self.game.pushMessage(self.createMessage(CORP_LOSE_CLICK, data=3))

    def handleTrashResource(self, event):
        # TODO
        pass


class Advanceable(Piece):
    def __init__(self, name, game, player):
        super().__init__(name, game, player)
        self.advance_self_action = '%s_%s' % (CORP_ADVANCE, self.uid)
        self.addZone('advances')

    def initializeSubscriptions(self):
        self.subscribe(self.handleAggregateAdvanceable, CORP_AGGREGATE_ADVANCEABLE)
        self.subscribe(self.handleAdvanceSelf, self.advance_self_action)

    def handleAggregateAdvanceable(self, event):
        event.events.append(self.advance_self_action)

    def handleAdvanceSelf(self, event):
        self.gainAdvances(event.amount)
    def gainAdvances(self, amount):
        for _ in range(amount):
            Piece('advance', self.game).move(self, 'advances')
