from core.gamepieces import Piece
from core.messaging import Message
from netrunner.events import *
from netrunner.baseactions import RunnerActions, CorporationActions


class OutOfClicksError(Exception): pass
class OutOfCreditsError(Exception): pass
class OutOfCardsError(Exception): pass
class NoTagsError(Exception): pass


class Player(Piece):
    def __init__(self, name, game, hand_size, clicks):
        super().__init__(name, game)
        self.hand_size = hand_size
        self.clicks = clicks
        self._initializeZones()

    def initializeSubscriptions(): pass

    def _initializeZones(self):
        self.addZone('rules')
        self.addZone('identity')
        self.addZone('credits')
        self.addZone('clicks')

    def getAmountCredits(self):
        return self.getAmount('credits')

    def refreshClicks(self, message):
        while self.getAmount('clicks') < self.clicks:
            Piece('click', self.game).move(self, 'clicks')

    def handleLoseClicks(self, message):
        self.loseClicks(message.amount if hasattr(message, 'amount') else 1)
    def loseClicks(self, amount):
        if self.getAmount('clicks') < amount:
            raise OutOfClicksError()
        for _ in range(amount):
            self['clicks'][0].removeFromPlay()
        self.game.log('The {0} loses {1} click{2}'.format(
            self, amount, 's' if amount > 1 else ''
        ))

    def handleGainCredits(self, message):
        self.gainCredits(message.amount if hasattr(message, 'amount') else 1)
    def gainCredits(self, amount):
        for _ in range(amount):
            Piece('credit', self.game).move(self, 'credits')
        self.game.log('The {0} gains {1} credit{2}'.format(
            self, amount, 's' if amount > 1 else ''
        ))

    def handleLoseCredits(self, message):
        self.loseCredits(message.amount if hasattr(message, 'amount') else 1)
    def loseCredits(self, amount):
        if self.getAmount('credits') < amount:
            raise OutOfCreditsError()
        for _ in range(amount):
            self['credits'][0].removeFromPlay()
        self.game.log('The {0} loses {1} credit{2}'.format(
            self, amount, 's' if amount > 1 else ''
        ))


class Runner(Player):
    def __init__(self, game):
        super().__init__('runner', game, hand_size=5, clicks=4)
        RunnerActions(self.game, self).move(self, 'rules')

    def initializeSubscriptions(self):
        self.subscribe(self.refreshClicks, BEGIN_RUNNER_ACTION)
        self.subscribe(self.handleLoseClicks, RUNNER_LOSE_CLICK)
        self.subscribe(self.handleGainCredits, RUNNER_GAIN_CREDIT)
        self.subscribe(self.handleLoseCredits, RUNNER_LOSE_CREDIT)
        self.subscribe(self.handleRemoveTag, RUNNER_REMOVE_TAG)

    def _initializeZones(self):
        super()._initializeZones()
        self.addZone('grip')
        self.addZone('stack')
        self.addZone('heap')
        self.addZone('programs')
        self.addZone('rig')
        self.addZone('resources')
        self.addZone('tags')

    def getAmountStack(self):
        return self.getAmount('stack')

    def getAmountTags(self):
        return self.getAmount('tags')

    def handleRemoveTag(self, message):
        self.loseCredits(2)
        self.removeTag()
    def removeTag(self):
        if self.getAmount('tags') < 1:
            raise NoTagsError()
        self['tags'][0].removeFromPlay()
        self.game.log('The runner removes a tag')

    def handleDraw(self, message):
        self.draw(message.amount if hasattr(message, 'amount') else 1)
    def draw(self, amount):
        for _ in range(min(amount, self.getAmountStack())):
            self['stack'][0].move(self, 'grip')


class Corporation(Player):
    def __init__(self, game):
        super().__init__('corporation', game, hand_size=5, clicks=3)
        CorporationActions(self.game, self).move(self, 'rules')

    def initializeSubscriptions(self):
        self.subscribe(self.refreshClicks, BEGIN_CORP_ACTION)
        self.subscribe(self.handleLoseClicks, CORP_LOSE_CLICK)
        self.subscribe(self.handleGainCredits, CORP_GAIN_CREDIT)
        self.subscribe(self.handleLoseCredits, CORP_LOSE_CREDIT)
        self.subscribe(self.handleAdvance, CORP_ADVANCE)

    def _initializeZones(self):
        super()._initializeZones()
        self.addZone('hq')
        self.addZone('rd')
        self.addZone('archives')

    def getAmountRD(self):
        return self.getAmount('rd')

    def handleDraw(self, message):
        self.draw(message.amount if hasattr(message, 'amount') else 1)
    def draw(self, amount):
        if amount > self.getAmountRd():
            self.game.winner = self.game.runner
            return
        for _ in range(amount):
            self['rd'][0].move(self, 'hq')

    def handleAdvance(self, message):
        # When advancing a card we need to ask the Corporation which card they
        # would like to advance. We don't need to handle payment, that's been
        # already taken care of. We also don't need to handle any triggers on
        # placing the advancement token, those should list to the actual token
        # placement
        # 1. Put a choice on the stack asking the user which card that they
        #    would like to advance. The choices should be all advanceable cards
        #    in the Corporation's play area. The choices should be named like
        #    'CORP_ADVANCE_[CARD_UID]'.
        # 2. All advanceable cards should listen for a message like
        #    'CORP_ADVANCE_[CARD_UID]', and respond by creating a new
        #    advancement token and moving it to themselves.
        for advanceable in self.game.send(Message(CORP_AGGREGATE_ADVANCEABLE, self.game, [])):
            self.game.pushChoice(Message(advanceable, self.game, message.amount))
