from netrunner.cards.corporation import Agenda
from netrunner.events import PAYMENT_MODIFY
from netrunner.definitions import ICE, SECURITY, REZ

class PriorityRequisition(Agenda):
    name = 'Priority Requisition'
    cost = 5
    score = 3
    subtypes = [SECURITY]
    text = '''When you score Priority Requistion, you
may rez a piece of ice ignoring all costs.'''
    flavour = '''"If it isn't in my terminal by six p.m., heads are going
to roll!"'''

    def score(self):
        super().score()
        self.subscribe(self.requisition, PAYMENT_MODIFY)
        self.game.corpChooseICEToRez()

    def requisition(self, event):
        if event.card.primary == ICE and event.action == REZ:
            event.free = True
            self.unsubscribe(PAYMENT_MODIFY)
