from netrunner.cards.corporation import Operation
from netrunner.events import CORP_GAIN_CREDIT
from netrunner.definitions import TRANSACTION

class HedgeFund(Operation):
    name = 'Hedge Fund'
    cost = 5
    subtypes = [TRANSACTION]
    text = 'Gain 9{credit}.'
    flavour = '''Hedge Fund. Noun. An ingenious device by which
the rich get richer while every other poor SOB is
losing his shirt.
\t-The Anarch's Dictionary, Volume Who's Counting?'''

    def play(self):
        message = self.createMessage(CORP_GAIN_CREDIT, data=9)
        self.game.pushMessage(message)
