from netrunner.cards.corporation import Asset
from netrunner.actions.corporation.melangeminingcorp \
    import MelangeMiningCorp as MMC


class MelangeMiningCorp(Asset):
    name = 'Melange Mining Corp.'
    cost = 1
    trash = 1
    text = '{click}, {click}, {click}: Gain 7{credit}.'
    flavour = '''"The mining bosses are worse than any downstalk
crime lords. Tri-Maf, 4K, Yak, I don't care what gangs
you got down there. In Heinlein there's just one law:
the He3 must flow."
\t-"Old" Rick Henry, escaped clone'''

    def initActions(self):
        super().initActions()
        self.actions[MMC.trigger] = MMC

    def rez(self):
        super().rez()
        self.actions[MMC.trigger].activate()
