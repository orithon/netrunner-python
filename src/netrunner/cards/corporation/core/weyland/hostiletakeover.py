from netrunner.cards.corporation import Agenda, Weyland
from netrunner.definitions import EXPANSION
from netrunner.events import CORP_GAIN_CREDIT, CORP_GAIN_BAD_PUBLICITY

class HostileTakeover(Agenda, Weyland):
    name = 'Hostile Takeover'
    cost = 2
    score = 1
    subtypes = [EXPANSION]
    text = '''When you score Hostile Takeover, gain 7{credit}
and take 1 bad publicity.'''
    flavour = 'There are going to be some changes around here.'

    def score(self):
        super().score()
        self.game.pushMessage(self.createMessage(CORP_GAIN_CREDIT, data=7))
        self.game.pushMessage(self.createMessage(CORP_GAIN_BAD_PUBLICITY))
