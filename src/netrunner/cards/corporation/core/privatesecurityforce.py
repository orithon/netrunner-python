from netrunner.cards.corporation import Agenda
from netrunner.actions.corporation.privatesecurityforce \
    import PrivateSecurityForce as PSF
from netrunner.definitions import SECURITY


class PrivateSecurityForce(Agenda):
    name = 'Private Security Force'
    cost = 4
    score = 2
    subtypes = [SECURITY]
    text = '''If the Runner is tagged, Private Security
Force gains "{click}: Do 1 meat damage."'''
    flavour = '''"Expensive? Not when you're protecting a fortune as
large as ours."'''

    def initActions(self):
        super().initActions()
        self.actions[PSF.trigger] = PSF

    def score(self):
        super().score()
        self.actions[PSF.trigger].activate()
