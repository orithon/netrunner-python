from netrunner.cards.corporation import Asset
from netrunner.events import BEGIN_CORP_TURN, CORP_GAIN_CREDIT
from netrunner.definitions import ADVERTISEMENT


class PadCampaign(Asset):
    name = 'PAD Campaign'
    cost = 2
    trash = 4
    subtypes = [ADVERTISEMENT]
    text = 'Gain 1{credit} when your turn begins'
    flavour = 'It is like the one you just bought, only better.'

    def rez(self):
        super().rez()
        self.subscribe(self.campaign, BEGIN_CORP_TURN)

    def campaign(self):
        message = self.createMessage(CORP_GAIN_CREDIT, data=1)
        self.game.pushMessage(message)
