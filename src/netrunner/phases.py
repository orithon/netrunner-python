from core.phases import Phase
from netrunner.events import *


GAME_START_PHASE = Phase('Game Start', GAME_START)
CORP_START_PHASE = Phase("Corporation's Start Phase",
                         BEGIN_CORP_DRAW, CORP_ABILITY_SCORE,
                         BEGIN_CORP_TURN, CORP_DRAW, END_CORP_DRAW)
CORP_ACTION_PHASE = Phase("Corporation's Action Phase",
                          BEGIN_CORP_ACTION, CORP_ACTION,
                          CORP_ABILITY_SCORE, END_CORP_ACTION)
CORP_DISCARD_PHASE = Phase("Corporation's Discard Phase",
                           BEGIN_CORP_DISCARD, CORP_DISCARD,
                           END_CORP_TURN, CORP_ABILITY, END_CORP_DISCARD)

RUNNER_START_PHASE = Phase("Runner's Start Phase",
                           BEGIN_RUNNER_START, RUNNER_ABILITY,
                           BEGIN_RUNNER_TURN, END_RUNNER_START)
RUNNER_ACTION_PHASE = Phase("Runner's Action Phase",
                            BEGIN_RUNNER_ACTION, RUNNER_ACTION,
                            RUNNER_ABILITY, END_RUNNER_ACTION)
RUNNER_DISCARD_PHASE = Phase("Runner's Discard Phase",
                             BEGIN_RUNNER_DISCARD, RUNNER_DISCARD,
                             END_RUNNER_TURN, RUNNER_ABILITY,
                             END_RUNNER_DISCARD)

BASE_PHASES = [CORP_START_PHASE, CORP_ACTION_PHASE, CORP_DISCARD_PHASE,
               RUNNER_START_PHASE, RUNNER_ACTION_PHASE, RUNNER_DISCARD_PHASE]

CORP_ABILITY_SCORE_PHASE = Phase("Corporation's Ability-Rez-Score Phase",
                                 CORP_TAKE_ABILITY_SCORE,
                                 RUNNER_TAKE_ABILITY)
CORP_ABILITY_PHASE = Phase("Corporation's Ability-Rez Phase",
                           CORP_TAKE_ABILITY, RUNNER_TAKE_ABILITY)
RUNNER_ABILITY_PHASE = Phase("Runner's Ability-Rez Phase",
                             RUNNER_TAKE_ABILITY, CORP_TAKE_ABILITY)
