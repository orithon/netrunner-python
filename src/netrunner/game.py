from core.game import Game
from core.messaging import Message

from netrunner.phases import GAME_START_PHASE, BASE_PHASES
from netrunner.players import Runner, Corporation
from netrunner.events import *


class Netrunner(Game):
    def initializePhases(self):
        self.phase = GAME_START_PHASE
        self.stack.push(self.phase.getMessages())
        for phase in BASE_PHASES:
            self.phaseManager.addPhase(phase)

    def initializePlayers(self):
        self.runner = Runner(self)
        self.corporation = Corporation(self)
        self.currentPlayer = self.corporation

    def initializeSubscriptions(self):
        print('DEBUG: Initializing subscriptions')
        self.subscribe(self.managePlayerChangeRunner, BEGIN_RUNNER_START)
        self.subscribe(self.managePlayerChangeCorporation, BEGIN_CORP_DRAW)
        self.subscribe(self.manageActionPhase, END_CORP_ACTION)
        self.subscribe(self.manageActionPhase, END_RUNNER_ACTION)

        self.subscribe(self.handleRunnerRun, RUNNER_RUN)

        self.subscribe(self.handleCorporationDraw, CORP_DRAW)
        self.subscribe(self.handleCorporationAdvance, CORP_ADVANCE)
        self.subscribe(self.handleCorporationPurge, CORP_PURGE_VIRUS)

    def getAmountVirusCounters(self):
        msg = Message(RUNNER_AGGREGATE_VIRUS, self, [])
        result = len(self.send(msg).data)
        print(result)
        return result

    def getAmountAdvanceable(self):
        msg = Message(CORP_AGGREGATE_ADVANCEABLE, self, [])
        result = len(self.send(msg).data)
        print(result)
        return result

    def managePlayerChangeRunner(self, message):
        self.currentPlayer = self.runner
        self.log('The Runner is now the current player')

    def managePlayerChangeCorporation(self, message):
        self.currentPlayer = self.corporation
        self.log('The Corporation is now the current player')

    def manageActionPhase(self, message):
        if self.currentPlayer.getAmount('clicks') > 0:
            self.stack.push(message)
            self.stack.push(self.phase.getBody())

    def handleRunnerRun(self, message):
        # TODO
        pass

    def handleCorporationDraw(self, message):
        # TODO
        pass

    def handleCorporationAdvance(self, message):
        # TODO
        pass

    def handleCorporationPurge(self, message):
        # TODO
        pass
