from core.gamepieces import Piece
from netrunner.events import (
    CORP_AGGREGATE_CREDITS, RUNNER_AGGREGATE_CREDITS, RUNNER_AGGREGATE_VIRUS
    )


class Token(Piece):
    def initializeSubscriptions(self):
        self.spend_action = 'SPEND_{}'.format(self.uid)
        self.game.subscribe(self.spend, self.spend_action)

    def spend(self, event):
        self.removeFromPlay()


class Credit(Token):
    def __init__(self, game, player=None):
        super().__init__('credit', game, player)

    def initializeSubscriptions(self):
        super().initializeSubscriptions()
        if self.player.name == 'corporation':
            self.game.subscribe(self.handleAggregateCredits, CORP_AGGREGATE_CREDITS)
        elif self.player.name == 'runner':
            self.game.subscribe(self.handleAggregateCredits, RUNNER_AGGREGATE_CREDITS)

    def handleAggregateCredits(self, event):
        event.events.append(self.spend_action)


class Virus(Token):
    def __init__(self, game, player=None):
        super().__init__('virus token', game, player)

    def initializeSubscriptions(self):
        super().initializeSubscriptions()
        self.game.subscribe(self.handleAggregateVirus, RUNNER_AGGREGATE_VIRUS)

    def handleAggregateVirus(self, event):
        event.events.append(self.spend_action)


class Advancement(Token):
    def __init__(self, game, player=None):
        super().__init__('advancement token', game, player)
