from netrunner.game import Netrunner
from core.game import GameWon, InvalidMoveError
from core.stack import Choice, Ordering

if __name__ == '__main__':
    game = Netrunner()
    event = None
    user_input = None

    while True:
        if user_input is not None:
            event = game.process(user_input)
            user_input = None
        else:
            event = game.process()

        if isinstance(event, (Choice, Ordering)):
            user_input = input('Pick Something ({}): '.format(str(event)))
            print('You chose: {}'.format(user_input))
